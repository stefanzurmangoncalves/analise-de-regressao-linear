if (!require("pacman")) install.packages("pacman")

pacman::p_load(
  tidyverse, data.table, magrittr,
  ggcorrplot, cowplot, RColorBrewer,
  readxl, scales, nortest
)
if (!require("PerformanceAnalytics")) install.packages("PerformanceAnalytics")
if (!require("FSA")) install.packages("FSA")
if (!require("dunn.test")) install.packages("dunn.test")
if (!require("DescTools")) install.packages("DescTools")

library(PerformanceAnalytics)
library(FSA)
library(dunn.test)
library(DescTools)

#Exercício 1:
dados <- data.frame(c(62.0,62.9,36.1,54.6,48.5,42.0,47.4,50.6,42.0,48.7,40.3,33.1,51.9,42.4,34.5,51.1,41.2,51.9,46.9),
                    c(1792,1666,995,1425,1396,1418,1362,1502,1256,1614,1189,913,1460,1124,1052,1347,1204,1867,1439))
dados <- rename(dados,  `taxa de metabolismo` = 2,
       `massa do corpo sem gordura` = 1)

#taxa de metabolismo
#boxplot
ggplot(dados) +
  aes(x=factor(""), y=`taxa de metabolismo`) +
  geom_boxplot(fill=c("gray"), width = 0.5) +
  guides(fill=FALSE) +
  stat_summary(fun="mean", geom="point", shape=23, size=3, fill="white")+
  labs(x="", y="taxa metabólica (em calorias)")
ggsave("TaxaMetabolica_Boxplot.pdf", width = 158, height = 93, units = "mm")
#valores resumo
summary(dados$`taxa de metabolismo`)
sd(dados$`taxa de metabolismo`)
skewness(dados$`taxa de metabolismo`, method="moment")
shapiro.test(dados$`taxa de metabolismo`)
qt(0.975,18)
t.test(dados$`taxa de metabolismo`)

#massa do corpo sem gordura
#boxplot
ggplot(dados) +
  aes(x=factor(""), y=`massa do corpo sem gordura`) +
  geom_boxplot(fill=c("gray"), width = 0.5) +
  guides(fill=FALSE) +
  stat_summary(fun="mean", geom="point", shape=23, size=3, fill="white")+
  labs(x="", y="massa do corpo sem gordura (em Kg)")
ggsave("Massa_Boxplot.pdf", width = 158, height = 93, units = "mm")
#valores resumo
summary(dados$`massa do corpo sem gordura`)
sd(dados$`massa do corpo sem gordura`)
skewness(dados$`massa do corpo sem gordura`, method="moment")
shapiro.test(dados$`massa do corpo sem gordura`)

#multi
#gráfico
ggplot(dados) +
  aes(x = `massa do corpo sem gordura`, y = `taxa de metabolismo`) +
  geom_point(colour = "black", size = 3) +
  labs(
    x = "Massa do Corpo sem Gordura",
    y = "Taxa Metabólica"
  ) +
  geom_smooth(method='lm', formula= y~x, se=FALSE)
ggsave("MassaVsMetabolismoDisp.pdf", width = 158, height = 93, units = "mm")
#testes
cor.test(dados$`massa do corpo sem gordura`,dados$`taxa de metabolismo`)
